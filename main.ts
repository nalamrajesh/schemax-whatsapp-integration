import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
var bodyParser = require('body-parser')

// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

dotenv.config();

const app: Express = express();
app.use(bodyParser.json())
const port = process.env.PORT;
require('./whatsapp-integration/message-controller')(app);

app.get('/', (req: Request, res: Response) => {
  res.send('Whatsapp integration is running');
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
