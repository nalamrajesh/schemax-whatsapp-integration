import { CustomerEnum } from "./customer-enum";
import { MessageTypeEnum } from "./message-type.enum";

export class MessageRequest {
    message: string;
    greeting: string;
    senderName: string;
    recipient: CustomerEnum;
    messageType: MessageTypeEnum
    pathWithFileName?: string;
    constructor(messageType: MessageTypeEnum, greeting: string, message: string, senderName: string, recipient: CustomerEnum, pathWithFileName?: string) {
        this.messageType = messageType;
        this.greeting = greeting;
        this.message = message;
        this.senderName = senderName;
        this.recipient = recipient;
        this.pathWithFileName = pathWithFileName;
    }
}