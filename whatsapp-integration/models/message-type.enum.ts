
export enum MessageTypeEnum {
    text = 'TEXT',
    image = 'IMAGE',
    pdf = 'PDF'
}