import express, { Express, Request, Response } from 'express';
import { MessageRequest } from './models/message.request';
import { CustomerEnum } from './models/customer-enum';
import { config } from 'dotenv';
import { appConfigs } from '../config';
import { MessageResponse } from './models/message.response';
const ultramsg = require('ultramsg-whatsapp-api');
const instance_id= "instance18516" // Ultramsg.com instance id
const ultramsg_token= appConfigs.keys.whatsappLicence;  // Ultramsg.com token
const api = new ultramsg(instance_id,ultramsg_token);

var fs = require('fs');

/**
 * 
 * var audio="https://file-example.s3-accelerate.amazonaws.com/audio/2.mp3"; 
   const response = await api.sendAudioMessage(postBody.recipient,audio);
 * 
 * 
 * var image="https://file-example.s3-accelerate.amazonaws.com/images/test.jpg"; 
    var priority=10;
    var referenceId="SDK"
    var nocache=false; 
    const response = await api.sendImageMessage(to,caption,image,priority,referenceId,nocache);
 */
module.exports = function(app){

    /**
     * send image as a personal message
     */
    app.post('/sendImageAsMessage', async (req: Request, res: Response) => {
        try {
            const postBody: MessageRequest = req.body;
            const filePathAndName = postBody.pathWithFileName;
            var image = '';
            await fs.readFile(filePathAndName, async function (err, data) {
                if (err) {
                    console.log(err);
                }
                image = data.toString('base64');
                var priority=10;
                var referenceId="SDK";
                var nocache=false;
                // send the image as whats app message
                const response = await api.sendImageMessage(postBody.recipient, postBody.message, image, priority, referenceId, nocache);
            });
        } catch(err: any){
            res.send(new MessageResponse(false, err.message));
        }
    });

    /**
     * sends the given message to the mentioned target whatsapp user
     */
    app.post('/sendPersonalMessage', async (req: Request, res: Response) => {
        try {
            const postBody: MessageRequest = req.body;
            // message(postBody.recipient, postBody.greeting, postBody.message, postBody.senderName, true, res);
            return await formatAndSendTextMessage(postBody.recipient, postBody.greeting, postBody.message, postBody.senderName);
        } catch(err: any) {
            console.log(err);
            res.send(new MessageResponse(false, err.message));
        }
    });

    /**
     * sends the given message to the mentioned target whatsapp group
     */
    app.post('/sendGroupMessage', (req: Request, res: Response) => {
        try {
            validateCreds(req.body, res);
            const postBody: MessageRequest = req.body;
            message(postBody.recipient, postBody.greeting, postBody.message, postBody.senderName, false, res);
        } catch(err: any){
            res.send(new MessageResponse(false, err.message));
        }
    });

    // testing api
    app.get('/test', async (req: Request, res: Response) => {
        await message('7207605260', 'Good Day', 'text=\naHello\nWorld', 'Schemaxtech', true, res);
    });


    async function formatAndSendTextMessage(recipient: string, greeting: string, message: string, sender: string) {
        const priority=10;
        const referenceId="SDK";
        let messageBody = '';
        if(greeting) {
            messageBody += "*"+greeting+"*\n";
        }
        messageBody += message;
        if(sender) {
            messageBody += "\n*From: "+sender+"*";
        }
        const response = await api.sendChatMessage(recipient, messageBody, priority, referenceId);
        return response;
    }

    function validateCreds(req: MessageRequest, res: Response) {
        if(!req) {
            throw new Error('Request object is invalid');
        }
        if(!req.message) {
            throw new Error('Message is empty');
        }
        const recipients = Object.keys(CustomerEnum);
        if(!recipients.includes(req.recipient)) {
            throw new Error('Recipient given is invalid');
        }
        const configuredKeys = Object.keys(appConfigs.recipients);
        if(!configuredKeys.includes(req.recipient)) {
            throw new Error('Provided recipient is not configured in the app');
        }
        return;
    }

    async function message(recipient: string, greeting: string, message: string, sender: string, personal: boolean, mainResponse: Response) {
        var qs = require("querystring");
        var http = require("https");
        let msg = '';
        if(greeting) {
            msg += "*"+greeting+"*\n";
        }
        msg+=message;
        if(sender) {
            msg += "\n*From: "+sender+"*";
        }

        var options = {
            method: "POST",
            hostname: "api.ultramsg.com",
            port: null,
            path: "/instance6559/messages/chat",
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            },
            timeout: 3000,
        };

        var req = await http.request(options, function (res, err) {
            var chunks: any[] = [];
            res.on("data", function (chunk) {
                chunks.push(chunk);
            });
            res.on("end", function () {
                var body = Buffer.concat(chunks);
            });
        });
        const prefix = personal ? "91" : "";
        const type = personal ? "@c.us" : "@g.us";
        // const to = prefix+recipient+type;
        console.log(recipient);
        req.write(qs.stringify({
            token: appConfigs.keys.whatsappLicence,
            to: recipient,//'919030069351@c.us',
            body: msg,
            priority: '1',
            referenceId: ''
        }));
        req.on('response', function(res) {
            console.log(res.statusCode);
            mainResponse.send(new MessageResponse(true, 'Message sent'));
        });
        req.on('error', function(res) {
            // console.log(res.statusCode);
            mainResponse.send(new MessageResponse(false, 'Message could not be sent'));
        });
        req.end();
    }

}