import { CustomerEnum } from "./whatsapp-integration/models/customer-enum";


export const appConfigs = {
    recipients: {
        [CustomerEnum.GRP_SANDHYA_AQUA_COMMON]: process.env.SANDHYA_AQUA_COMMON ?? '9030069351',
        [CustomerEnum.GRP_SCHEMAX_COMMON]: process.env.SCHEMAX_COMMON ?? '9030069351',
        [CustomerEnum.PER_SANDHYA_AQUA_COMMON_ROLE1]: process.env.PER_SANDHYA_AQUA_COMMON_ROLE1 ?? '9030069351',
        [CustomerEnum.PER_SANDHYA_AQUA_COMMON_ROLE2]: process.env.PER_SANDHYA_AQUA_COMMON_ROLE1 ?? '9030069351',
    },
    port: {
        port: process.env.port ?? 3000
    },
    keys: {
        whatsappLicence: process.env.WP_LICENSE ?? "6wz26r3m6yf049en"
    }
}